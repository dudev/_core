<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient',
    ),
  ),
  'yiisoft/yii2-sphinx' => 
  array (
    'name' => 'yiisoft/yii2-sphinx',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/sphinx' => $vendorDir . '/yiisoft/yii2-sphinx',
    ),
  ),
  'yiisoft/yii2-redis' => 
  array (
    'name' => 'yiisoft/yii2-redis',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/redis' => $vendorDir . '/yiisoft/yii2-redis',
    ),
  ),
  'yiisoft/yii2-elasticsearch' => 
  array (
    'name' => 'yiisoft/yii2-elasticsearch',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/elasticsearch' => $vendorDir . '/yiisoft/yii2-elasticsearch',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'vova07/yii2-imperavi-widget' => 
  array (
    'name' => 'vova07/yii2-imperavi-widget',
    'version' => '1.2.10.0',
    'alias' => 
    array (
      '@vova07/imperavi' => $vendorDir . '/vova07/yii2-imperavi-widget/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'janisto/yii2-timepicker' => 
  array (
    'name' => 'janisto/yii2-timepicker',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@janisto/timepicker' => $vendorDir . '/janisto/yii2-timepicker/src',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.2.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-popover-x' => 
  array (
    'name' => 'kartik-v/yii2-popover-x',
    'version' => '1.3.3.0',
    'alias' => 
    array (
      '@kartik/popover' => $vendorDir . '/kartik-v/yii2-popover-x',
    ),
  ),
  'kartik-v/yii2-editable' => 
  array (
    'name' => 'kartik-v/yii2-editable',
    'version' => '1.7.2.0',
    'alias' => 
    array (
      '@kartik/editable' => $vendorDir . '/kartik-v/yii2-editable',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.0.4.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid',
    ),
  ),
  'johnitvn/yii2-ajaxcrud' => 
  array (
    'name' => 'johnitvn/yii2-ajaxcrud',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@johnitvn/ajaxcrud' => $vendorDir . '/johnitvn/yii2-ajaxcrud/src',
    ),
    'bootstrap' => 'johnitvn\\ajaxcrud\\Bootstrap',
  ),
  'johnitvn/yii2-user-plus' => 
  array (
    'name' => 'johnitvn/yii2-user-plus',
    'version' => '1.0.0.0-RC',
    'alias' => 
    array (
      '@johnitvn/userplus' => $vendorDir . '/johnitvn/yii2-user-plus/src',
    ),
    'bootstrap' => 'johnitvn\\userplus\\Bootstrap',
  ),
  'johnitvn/yii2-rbac-plus' => 
  array (
    'name' => 'johnitvn/yii2-rbac-plus',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@johnitvn/rbacplus' => $vendorDir . '/johnitvn/yii2-rbac-plus/src',
    ),
    'bootstrap' => 'johnitvn\\rbacplus\\Bootstrap',
  ),
  'filsh/yii2-oauth2-server' => 
  array (
    'name' => 'filsh/yii2-oauth2-server',
    'version' => '2.0.1.0',
    'alias' => 
    array (
      '@filsh/yii2/oauth2server' => $vendorDir . '/filsh/yii2-oauth2-server',
    ),
    'bootstrap' => 'filsh\\yii2\\oauth2server\\Bootstrap',
  ),
);
